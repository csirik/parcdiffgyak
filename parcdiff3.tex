\documentclass[a4paper,10pt,leqno]{article}
\renewcommand{\baselinestretch}{1.1} 
\tolerance=2000
\emergencystretch=10pt
\usepackage[a4paper, margin=0.5in]{geometry}

\def\magyarOptions{defaults=safest}

\usepackage{eltefont}
\usepackage{eltehun}
\usepackage{eltesymb}
\usepackage{eltethmhun}

\usepackage{mathtools}

\usepackage{enumerate}
\usepackage{quoting}

\usepackage[dvipsnames]{xcolor}
\usepackage{hyperref}
\usepackage{cleveref}

\usepackage[explicit,pagestyles]{titlesec}

\usepackage{stackengine}
\usepackage{graphicx}
\def\asterism{\par\vspace{1em}{\centering\scalebox{1.5}{%
  \stackon[-0.5pt]{\bfseries*~*}{\bfseries*}}\par}\vspace{.5em}\par}


\newcommand{\myshade}{85}
\colorlet{mylinkcolor}{blue}
\colorlet{mycitecolor}{YellowOrange}
\colorlet{myurlcolor}{Aquamarine}

\hypersetup{
  linkcolor  = mylinkcolor!\myshade!black,
  citecolor  = mycitecolor!\myshade!black,
  urlcolor   = myurlcolor!\myshade!black,
  colorlinks = true,
  pdftitle={Parciális differenciálegyenletek gyakorlat},
  pdfauthor={Csirik Mih\'aly},
  pdfsubject={Your subject here},
  pdfkeywords={keyword1, keyword2},
  bookmarksnumbered=true,     
  bookmarksopen=true,         
  bookmarksopenlevel=1,       
  pdfstartview=Fit,           
  pdfpagemode=UseOutlines,    % this is the option you were lookin for
  pdfpagelayout=TwoPageRight,
}



%\usepackage{fouriernc}

\newcommand{\Rs}{\Real^3}
\newcommand{\fie}[1]{\mathbf{#1}}
\newcommand{\ffie}[1]{\widehat{\mathbf{#1}}}
\newcommand{\dua}[2]{\langle #1, #2 \rangle}

\renewcommand{\cdot}{\bm{\cdot}}
\newcommand{\uni}[1]{\mathcal{U}(#1)}
\newcommand{\lmap}[2]{\mathcal{L}\left(#1,#2\right)}
\newcommand{\ltrf}[1]{\mathcal{L}(#1)}
\newcommand{\ltrfcn}{\mathcal{L}(\Cn)}
\newcommand{\mlin}[3]{\mathcal{L}^{#1}\left(#2,#3\right)}
\newcommand{\mlinp}[3]{\mathcal{L}^{#1}(#2,#3)}
\newcommand{\tens}[3]{\mathcal{T}_{#1}^{#2}(#3)}
\newcommand{\Arr}[2]{#1\longrightarrow #2}

\newcommand{\Symt}[2]{S^{#1}(#2)}
\newcommand{\Asymt}[2]{{\bigwedge\!}^{#1}(#2)}
\newcommand{\exter}[1]{\bigwedge(#1)}
\newcommand{\Sym}{\mathcal{S}}
\newcommand{\Asym}{\mathcal{A}}
\newcommand{\SyGr}[1]{\Sigma_{#1}}
\newcommand{\dual}[1]{#1^*}

\newcommand{\prodsp}[1]{\mathcal{#1}}

\newcommand{\thalf}{\tfrac{1}{2}}

\newcommand{\DForms}{\Omega^*}
\newcommand{\DFormsd}[1]{\Omega^{#1}}
\newcommand{\Cinf}{C^\infty}

\newcommand{\IInt}[1]{\mathop{\int\!\!\!\!\!\int}\limits_{\!\!\!\!\!\!#1}\!}

\DeclareMathOperator{\ch}{ch}
\DeclareMathOperator{\arctg}{arctg}
\DeclareMathOperator{\graph}{graph}


\begin{document}
\pagenumbering{gobble}
\vspace{-1cm}
\begin{center}
{\LARGE\textbf{Parciális differenciálegyenletek}}

\vspace{1em}

{\Large\textbf{3. Gyakorlat}}
\end{center}

\begin{Exercise}
Hol elliptikus, parabolikus, hiperbolikus?
\begin{align*}
&\text{(a)}\quad \lapl u\quad\quad\text{(b)}\quad u_{t}-\lapl u \quad\quad  \text{(c)}\quad u_{tt}-\lapl u \quad\quad  \text{(d)}\quad u_{xx}-xu_{yy}\;\;\text{(\emph{Euler--Tricomi})}\\
&\text{(e)}\quad u_{11}+6u_{12}+u_{22}\quad\quad  \text{(f)}\quad (x_1+x_2)u_{11} + 2\sqrt{x_1x_2} u_{12} + (x_1+x_2)u_{22}
\end{align*}
\end{Exercise}

\begin{Exercise}
Transzformáljuk kanonikus alakra az alábbi egyenleteket!
\begin{align*}
&\text{(a)}\quad u_{11}+2u_{12}+u_{22}+u_1+u=x_1-x_2 \quad\quad\text{(b)}\quad  u_{11}+u_{22}+u_1+u_2+u=x_1x_2\quad\quad\text{(c)}\quad  4u_{12}+2u_2+u=x+y
\end{align*}
\end{Exercise}


\vspace{1em}
\asterism
\vspace{1em}

\begin{Exercise}
Legyen $\Omega\subset\Rn$ nyílt és legyen $\mathcal{D}(\Omega)=C^\infty_c(\Omega)$ az induktív topológiával ellátva. Hova konvergálnak $\mathcal{D}(\Omega)$-ban az alábbi sorozatok,
ahol $\phi\in\mathcal{D}(\Omega)$ rögzített és $\ell\to\infty$?
\begin{align*}
&\text{(a)}\quad \frac{1}{\ell}\phi(\fie{x})\quad\quad\text{(b)}\quad \frac{1}{\ell}\phi\Big(\frac{\fie{x}}{\ell}\Big)\quad\quad\text{(c)}\quad \frac{1}{\ell}\phi(\ell\fie{x})
\quad\quad\text{(d)}\quad \phi(\fie{x}-\ell\fie{e}_1)\quad\quad\text{(e)}\quad \phi\Big(\fie{x}-\frac{1}{\ell}\fie{e}_1\Big)
\end{align*}
\end{Exercise}

\begin{Exercise}
Rögzített $a\in\Rn$ esetén tekintsük a $\delta_a\in\mathcal{D}(\Rn)^*$ \emph{Dirac-$\delta$-disztribúciót}, amelyet a $\dua{\delta_a}{\phi}=\phi(a)$ reláció definiál
minden $\phi\in\mathcal{D}(\Rn)$ esetén.
\begin{enumerate}[(a)]
\item Mutassuk meg, hogy $\delta_a$ nulladrendű disztribúció!
\item Mutassuk meg, hogy nincs olyan $f\in\Lloc(\Rn)$, amellyel $\delta_a=T_f$, ahol $T_f$ jelöli az $f$ által generált reguláris disztribúciót!
\item Adjunk meg egy $\mu$ Borel-mértéket $\Rn$-en, amellyel $\delta_a=T_\mu$, ahol  $T_\mu$ jelöli a $\mu$ által generált reguláris disztribúciót!
\item Hasonlóan, egy $M\subset\Rn$ $(n-1)$-dimenziós sima felület esetén $\dua{\delta_M}{\phi}=\int_M \phi$ disztribúció. Mi generálja?
\end{enumerate}
\end{Exercise}

\begin{Exercise}
Lássuk be az alábbi disztribúciós azonosságokat, ahol $H=\chi_{(0,+\infty)}$!
\begin{align*}
\text{(a)}\quad |\cdot|'=\sgn \quad\quad \text{(b)}\quad \sgn'=2\delta_0 \quad\quad \text{(c)}\quad H'=\delta_0 
\end{align*}
\end{Exercise}

\begin{Exercise}
Legyen $T(\phi)=\int_0^{+\infty} \phi(0,y),dy$, ahol $\phi\in\mathcal{D}(\Omega)$. Mutassuk meg, hogy
\begin{enumerate}[(a)]
\item $T\in\mathcal{D}(\Real^2)^*$,
\item $D_2T=\delta_0$,
\item van olyan $f\in\Lloc(\Real^2)$, hogy $T=D_1T_f$!
\end{enumerate}
\end{Exercise}

\begin{Exercise}
Definiáljuk a \emph{Cauchy-főértéket}, mint disztribúciót a 
\[
P(\phi)=\lim_{\epsilon\to 0}\int_{|x|\ge\epsilon} \frac{\phi(x)}{x}\,dx
\]
utasítással ($\phi\in\mathcal{D}(\Omega)$)! Mutassuk meg, hogy $P\in\mathcal{D}(\Omega)^*$! [Segítség: számoljuk ki a $(\log|x|)'$ disztribúciót!]
\end{Exercise}

\begin{Exercise}
Legyen $A\in\Real^{n\times n}$ reguláris, és adott $T\in\mathcal{D}(\Omega)^*$ disztribúció esetén tekintsük a
\[
(T\circ A)(\phi)=(\det A)^{-1} T(\phi\circ A^{-1})
\] funkcionált. Igazoljuk, hogy $T\circ A\in \mathcal{D}(\Omega)^*$! Lássuk be azt is, hogy
bármely $f\in \Lloc(\Omega)$ esetén $T_f\circ A=T_{f\circ A}$!
\end{Exercise}

\end{document}
