\documentclass[a4paper,10pt,leqno]{article}
\renewcommand{\baselinestretch}{1.1} 
\tolerance=2000
\emergencystretch=10pt
\usepackage[a4paper, margin=0.5in]{geometry}

\def\magyarOptions{defaults=safest}

\usepackage{eltefont}
\usepackage{eltehun}
\usepackage{eltesymb}
\usepackage{eltethmhun}

\usepackage{mathtools}

\usepackage{enumerate}
\usepackage{quoting}

\usepackage[dvipsnames]{xcolor}
\usepackage{hyperref}
\usepackage{cleveref}

\usepackage[explicit,pagestyles]{titlesec}

\usepackage{stackengine}
\usepackage{graphicx}
\def\asterism{\par\vspace{1em}{\centering\scalebox{1.5}{%
  \stackon[-0.5pt]{\bfseries*~*}{\bfseries*}}\par}\vspace{.5em}\par}


\newcommand{\myshade}{85}
\colorlet{mylinkcolor}{blue}
\colorlet{mycitecolor}{YellowOrange}
\colorlet{myurlcolor}{Aquamarine}

\hypersetup{
  linkcolor  = mylinkcolor!\myshade!black,
  citecolor  = mycitecolor!\myshade!black,
  urlcolor   = myurlcolor!\myshade!black,
  colorlinks = true,
  pdftitle={Parciális differenciálegyenletek gyakorlat},
  pdfauthor={Csirik Mih\'aly},
  pdfsubject={Your subject here},
  pdfkeywords={keyword1, keyword2},
  bookmarksnumbered=true,     
  bookmarksopen=true,         
  bookmarksopenlevel=1,       
  pdfstartview=Fit,           
  pdfpagemode=UseOutlines,    % this is the option you were lookin for
  pdfpagelayout=TwoPageRight,
}



%\usepackage{fouriernc}

\newcommand{\Rs}{\Real^3}
\newcommand{\fie}[1]{\mathbf{#1}}
\newcommand{\ffie}[1]{\widehat{\mathbf{#1}}}
\newcommand{\dua}[2]{\langle #1, #2 \rangle}

\renewcommand{\cdot}{\bm{\cdot}}
\newcommand{\uni}[1]{\mathcal{U}(#1)}
\newcommand{\lmap}[2]{\mathcal{L}\left(#1,#2\right)}
\newcommand{\ltrf}[1]{\mathcal{L}(#1)}
\newcommand{\ltrfcn}{\mathcal{L}(\Cn)}
\newcommand{\mlin}[3]{\mathcal{L}^{#1}\left(#2,#3\right)}
\newcommand{\mlinp}[3]{\mathcal{L}^{#1}(#2,#3)}
\newcommand{\tens}[3]{\mathcal{T}_{#1}^{#2}(#3)}
\newcommand{\Arr}[2]{#1\longrightarrow #2}

\newcommand{\Symt}[2]{S^{#1}(#2)}
\newcommand{\Asymt}[2]{{\bigwedge\!}^{#1}(#2)}
\newcommand{\exter}[1]{\bigwedge(#1)}
\newcommand{\Sym}{\mathcal{S}}
\newcommand{\Asym}{\mathcal{A}}
\newcommand{\SyGr}[1]{\Sigma_{#1}}
\newcommand{\dual}[1]{#1^*}

\newcommand{\prodsp}[1]{\mathcal{#1}}

\newcommand{\thalf}{\tfrac{1}{2}}

\newcommand{\DForms}{\Omega^*}
\newcommand{\DFormsd}[1]{\Omega^{#1}}
\newcommand{\Cinf}{C^\infty}

\newcommand{\IInt}[1]{\mathop{\int\!\!\!\!\!\int}\limits_{\!\!\!\!\!\!#1}\!}

\DeclareMathOperator{\ch}{ch}
\DeclareMathOperator{\arctg}{arctg}
\DeclareMathOperator{\graph}{graph}


\begin{document}
\pagenumbering{gobble}
\vspace{1cm}
\begin{center}
{\LARGE\textbf{Parciális differenciálegyenletek}}

\vspace{1em}

{\Large\textbf{4. Gyakorlat}}
\end{center}

\begin{Exercise}
(\emph{Laplace-operátor}) Legyen $n\ge 2$ és tekintsük a $\lapl$ operátor alapmegoldását:
\[
G(\fie{x})=\begin{cases}
\displaystyle{-\frac{1}{\sigma_2} \log \frac{1}{r}} & n=2\\
\displaystyle{-\frac{1}{(n-2)\sigma_n} \frac{1}{r^{\,n-2}}} & n\ge 3
\end{cases}
\]
ahol $\sigma_n$ az $n$-dimenziós egységgömb felszíne és $r=|\fie{x}|$. Gyakran egyszerűen $G(r)$-et írunk $G(\fie{x})$ helyett.  Igazoljuk az alábbiakat!
\begin{enumerate}[(a)]
\item $D_kG(\fie{x})=\dfrac{1}{\sigma_n} \dfrac{x_k}{r^n}$, $D_{k\ell}G(\fie{x})=\dfrac{1}{\sigma_n} \dfrac{1}{r^{\,n+2}}(r^{\,2}\delta_{k\ell}-nx_kx_\ell)$
\item $G$ harmonikus $\Rn\setdif\{0\}$-n, azaz $\lapl G(\fie{x})=0$ minden $\fie{x}\neq 0$ esetén.
\item $D_\nu G(\fie{x})=G'(r)$, ha $|\fie{x}|=r$ és $\nu\in\Rn$ az $\fie{x}\in S_r:=S(0,r)$ pontbeli, kifelé mutató normálvektor.
\item $\int_{S_r} D_\nu G=1$
\item $G\in \Lloc(\Rn)$, $DG\in\Lloc(\Rn,\Rn)$ és $D^2G\in\Lloc(\Rn,\Real^{n\times n})$
\item $\displaystyle{\int_{S_R} \frac{d\bm{\xi}}{|\bm{\xi}-\fie{x}|}=4\pi\begin{cases} R & r<R\\ \dfrac{R^2}{r} & r\ge R \end{cases}}$,
\quad\quad$\displaystyle{\int_{B_R} \frac{d\fie{y}}{|\fie{y}-\fie{x}|}= 2\pi\begin{cases} R^2 - \dfrac{r^{\,2}}{3} & r<R\\ \dfrac{2}{3} \dfrac{R^3}{r} & r\ge R \end{cases}}$
\item $\lapl G=\delta_0$ ($\mathcal{D}(\Rn)^*$-ban)
\end{enumerate}
\end{Exercise}

\begin{Exercise}
(\emph{Diffúziós- vagy hőoperátor}) Legyen $d\ge 2$ és tekintsük a $D_t-\lapl$ operátor alapmegoldását:
\[
K_t(\fie{x})=(4\pi t)^{-n/2} e^{-\tfrac{r^{\,2}}{4t}},
\]
ahol $t>0$, $\fie{x}\in\Rn$ és $r=|\fie{x}|$. Igazoljuk az alábbiakat!
\begin{enumerate}[(a)]
\item $D_tK_t(\fie{x})-\lapl K_t(\fie{x})=0$ minden $t>0$ és $\fie{x}\in\Rn$ esetén.
\item $K_t(\fie{x})=t^{-n/2}K_1(t^{-1/2}\fie{x})$
\item  $\int_{\Rn} K_t(\fie{x})\,d\fie{x}=1$ ($t>0$)
\item $K_t(\fie{x})\to 0$, ha $\fie{x}\neq 0$ és $K_t(0)\to+\infty$ ($t\searrow 0$) 
\item $K\in\Lloc(\Real\times\Rn)$
\item $D_tK_t-\lapl K_t=\delta_0$ ($\mathcal{D}(\Real\times\Rn)^*$-ban)
\end{enumerate}
\end{Exercise}

% \vspace{1em}
% \asterism
% \vspace{1em}

\newpage

\vspace{1em}
\begin{center}
\textbf{Fontos integráltételek}
\end{center}
\vspace{1em}


\noindent Az alábbiakban legyen $\Omega\subset\Rn$ korlátos, $C^1$-peremű tartomány.

\noindent\textbf{Gauss-tétel.} Legyen $\Map{\fie{u}=(u_1,\ldots,u_n)}{\Omega}{\Rn}$ $C^1$-vektormező. Ekkor
\[
\int_\Omega D_ju_i=\int_{\partial\Omega} u_i\nu_j,\quad\text{azaz}\quad \int_\Omega D\fie{u}=\int_{\partial\Omega} \fie{u}\otimes \bm{\nu},
\]
ahol $\Map{\bm{\nu}}{\partial\Omega}{\Rn}$ az $\partial\Omega$ kifelé mutató normális irányú, egység hosszú vektormezője.
Ebből összegzéssel kapjuk a szokásos alakot:
\[
\int_\Omega \dive \fie{u}=\int_{\partial\Omega} \fie{u}\cdot\bm{\nu}.
\]

\noindent\textbf{Első Green-formula.} Legyen $\fie{u}\in C^1(\overline{\Omega},\Rn)\cap C^2(\Omega,\Rn)$ és $v\in C(\overline{\Omega})\cap C^1(\Omega)$. Ekkor
\[
\int_\Omega (\dive\fie{u}) v=\int_{\partial\Omega} \fie{u}\cdot\bm{\nu}v - \int_\Omega \fie{u}\cdot Dv.
\]

\noindent\textbf{Második Green-formula.}  Legyenek $\Map{u,v}{\Omega}{\Real}$ $C^2$-osztályú skalármezők. Ekkor
\[
\int_\Omega v\lapl u=\int_{\partial\Omega} vD_\nu u - \int_\Omega Du\cdot Dv,
\]
és
\[
\int_\Omega (u\lapl v - v\lapl u)=\int_{\partial\Omega} (uD_\nu v - v D_\nu u).
\]

\noindent\textbf{Green-féle reprezentációs formula.} Legyen $n=3$ és $\Map{u}{\Omega}{\Real}$ $C^2$-osztályú skalármező. Ekkor
\[
\forall \fie{x}\in\Omega \;:\; 
(n-2)\sigma_nu(\fie{x})=\int_{\partial\Omega}\Bigg(\frac{1}{|\bm{\xi}-\fie{x}|^{n-2}}D_\nu u(\bm{\xi}) - u(\bm{\xi})D_\nu\frac{1}{|\bm{\xi}-\fie{x}|^{n-2}}\Bigg) \,d\bm{\xi}  - \int_\Omega \frac{\lapl u(\fie{y})\,d\fie{y}}{|\fie{y}-\fie{x}|^{n-2}} 
\]

\noindent\textbf{Coarea formula.} Legyen $\Map{u}{\Rn}{\Real}$ integrálható. Jelölje $\lambda^n$ az $n$-dimenziós Lebesgue-mértéket. Ekkor
\[
\int_{\Rn} u\,d\lambda^n = \int_0^{+\infty} \Bigg(\int_{S(0,\rho)} u\,d\lambda^{n-1}\Bigg)\,d\rho
\]
és
\[
\frac{d}{d\rho} \Bigg(\int_{B(0,\rho)} u\,d\lambda^n\Bigg) = \int_{S(0,\rho)} u\,d\lambda^{n-1}.
\]

\end{document}
